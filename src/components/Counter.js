import React, { Component } from 'react'

export class Counter extends Component {
    render() {
        return (
            <div className='counter'>
                <button className='add-btn' onClick={() => this.props. increment(this.props.counter.id)}>+</button>
        <div className='counter-box'>{this.props.counter.value}</div>
                <button className='add-btn' onClick={() =>this.props. decrement(this.props.counter.id)}>-</button>
                <button className='reset' onClick={() =>this.props. reset(this.props.counter.id)}>Reset</button>
                <button className='delete' onClick={() =>this.props. delete(this.props.counter.id)}>Delete</button>
            </div>
        )
    }
}

export default Counter
