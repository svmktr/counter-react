import React, { Component } from 'react'
import Counter from '../components/Counter'
import uuid from 'uuid'



class Counters extends Component {
    state = {
        counter: JSON.parse(localStorage.getItem('counters')) || [{
            value: 0,
            id: uuid.v4(),
        }]
    }



    setLocalStorage = () => {
        localStorage.setItem('counters', JSON.stringify(this.state.counter));

    }

    //   setLocalStorage()

    increment = (id) => {
        this.setState({
            counter: this.state.counter.map(item => {
                if (item.id === id)
                    item.value += 1
                // console.log(item.id)
                return item
            })
        })
        // console.log(this.state.counter)
    }


    decrement = (id) => {
        this.setState({
            counter: this.state.counter.map(item => {
                if (item.id === id && item.value > 0)
                    item.value -= 1
                // console.log(item.id)
                return item
            })
        })
    }

    reset = (id) => {
        this.setState({
            counter: this.state.counter.map(item => {
                if (item.id === id)
                    item.value = 0
                // console.log(item.id)
                return item
            })
        })
    }

    addCounter = () => {
        let newCounter = {
            value: 0,
            id: uuid.v4(),
        }
        this.setState({ counter: [...this.state.counter, newCounter] })
    }

    deleteCounter = (id) => {
        this.setState({
            counter: [...this.state.counter.filter(
                item =>
                    item.id !== id
            )
            ]
        })
    }


    render() {
        this.setLocalStorage()
        return (
            <div >
                <button onClick={this.addCounter}>+add</button>
                {this.state.counter.map(item => (
                    // console.log(item)
                    <Counter increment={this.increment} key={item.id} counter={item} decrement={this.decrement} reset={this.reset} delete={this.deleteCounter} />
                ))}

            </div>
        )
    }
}

export default Counters
