import React from 'react';
import './App.css';
import Counters from '../src/components/Counters'

function App() {
  return (
    <div className="App">
      <Counters/>
    </div>
  );
}

export default App;
